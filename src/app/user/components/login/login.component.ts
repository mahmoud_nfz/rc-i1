import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {Router} from "@angular/router"


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  // constructor() { }

  ngOnInit(): void {
  }
  
  constructor(private router: Router,private fb: FormBuilder) { }
    topErrorMessage!: string;
    errorMessage ="This field should not be empty";
    public loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
  });

  login(): void {
    // console.warn('I have completed the fields');
    if(this.loginForm.value.username != "" && this.loginForm.value.password != "")
      this.router.navigate(['/internship',this.loginForm.value.username]);
    else
      this.topErrorMessage="Please fill the fields";
    
  }

}
