import { Component, OnInit } from '@angular/core';
import { DrawerService } from '../services/drawer.service';


@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  constructor(private drawerService: DrawerService) { }

  ngOnInit(): void {

  }
  openNav(): void{
    this.drawerService.toggle();
  }

}
