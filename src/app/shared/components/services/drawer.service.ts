import { Injectable } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';

@Injectable({
    providedIn: 'root'
})
export class DrawerService {
    private drawer: MatDrawer|undefined;
    constructor(){
        // this.drawer = new MatDrawer() ;
    }

    setDrawer(drawer: MatDrawer|undefined) {
        this.drawer = drawer;
    }

    toggle(): void {
        this.drawer?.toggle();
    }

}
