import { Component } from '@angular/core';
import { SidenavComponent } from './shared/components/sidenav/sidenav.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'test';
  toggled !: boolean ;
  ngOnInit(): void {
    this.toggled = true ;
  }
}
